import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const FolderPageModule = import('./folder/folder.module');
const MyTeamsPageModule = import('./my-teams/my-teams.module');

const routes: Routes = [
  {
    path: '',
    redirectTo: 'my-teams',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: async () => {
      const m = await FolderPageModule;
      return m.FolderPageModule;
    }
  },
  {
    path: 'my-teams',
    loadChildren: async () => {
      const m = await MyTeamsPageModule;
      return m.MyTeamsPageModule;
    }
  },
  {
    path: 'tournaments',
    loadChildren: () => import('./tournaments/tournaments.module').then( m => m.TournamentsPageModule)
  },
  {
    path: 'teams',
    loadChildren: () => import('./teams/teams.module').then( m => m.TeamsPageModule)
  },
  {
    path: 'teams/:id',
    loadChildren: () => import('./team-detail/team-detail.module').then( m => m.TeamDetailPageModule)
  },
  {
    path: 'game',
    loadChildren: () => import('./game/game.module').then( m => m.GamePageModule)
  },
  {
    path: 'team-home',
    loadChildren: () => import('./team-home/team-home.module').then( m => m.TeamHomePageModule)
  },
  {
    path: 'standings',
    loadChildren: () => import('./standings/standings.module').then( m => m.StandingsPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
