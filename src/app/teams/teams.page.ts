import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { TeamDataServiceService } from '../services/team-data-service.service';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.page.html',
  styleUrls: ['./teams.page.scss'],
})
export class TeamsPage implements OnInit {
  teams = [];

  constructor(private nav: NavController, private teamService: TeamDataServiceService) { }

  ngOnInit() {
    this.teams = this.teamService.teams;
  }

  itemTapped(event: MouseEvent, team: any) {
    // this.nav.navigateForward(`teams/${team.id}`);
    this.nav.navigateForward(`/team-home/team-detail/${team.id}`);
  }
}
