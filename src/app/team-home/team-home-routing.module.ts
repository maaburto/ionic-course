import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TeamHomePage } from './team-home.page';
import { TeamDetailPage } from '../team-detail/team-detail.page';
import { StandingsPage } from '../standings/standings.page';

const routes: Routes = [
  {
    path: '',
    component: TeamHomePage,
    children: [
      {
        path: 'team-detail',
        children: [
          {
            path: ':id',
            component: TeamDetailPage,
          }
        ]
      },
      {
        path: 'standings',
        children: [
          {
            path: '',
            component: StandingsPage,
          }
        ]
      },
      {
        path: '',
        redirectTo: 'standings',
        pathMatch: 'full'
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TeamHomePageRoutingModule { }
