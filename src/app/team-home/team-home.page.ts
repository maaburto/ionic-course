import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-team-home',
  templateUrl: './team-home.page.html',
  styleUrls: ['./team-home.page.scss'],
})
export class TeamHomePage implements OnInit {
  constructor(private route: ActivatedRoute, private nav: NavController) {}

  ngOnInit() {
    this.route.params.forEach((param: Params) => {
      console.log(param.id);
    });
  }

  goHome() {
    this.nav.navigateRoot('my-teams');
  }
}
