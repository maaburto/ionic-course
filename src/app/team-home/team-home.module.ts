import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TeamHomePageRoutingModule } from './team-home-routing.module';

import { TeamHomePage } from './team-home.page';
import { TeamDetailPageModule } from '../team-detail/team-detail.module';
import { StandingsPageModule } from '../standings/standings.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TeamHomePageRoutingModule,
    TeamDetailPageModule,
    StandingsPageModule,
  ],
  declarations: [
    TeamHomePage,
  ]
})
export class TeamHomePageModule {}
