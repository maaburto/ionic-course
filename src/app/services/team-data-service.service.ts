import { Injectable } from '@angular/core';

const TEAMS = [{
  id: 1, name: 'HC Elite',
}, {
  id: 2, name: 'Team Takeover',
}, {
  id: 3, name: 'DC Thunder',
}];

@Injectable()
export class TeamDataServiceService {
  teams = [];
  constructor() {
    this.teams = TEAMS;
  }

  getTeam(id: number): any {
    return this.teams.find(team => team.id === id);
  }
}
