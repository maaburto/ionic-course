import { TestBed } from '@angular/core/testing';

import { TeamDataServiceService } from './team-data-service.service';

describe('TeamDataServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TeamDataServiceService = TestBed.get(TeamDataServiceService);
    expect(service).toBeTruthy();
  });
});
