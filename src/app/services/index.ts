import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamDataServiceService } from './team-data-service.service';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [],
  providers: [
    TeamDataServiceService,
  ]
})
export class ServiceModule {}
