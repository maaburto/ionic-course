import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { TeamDataServiceService } from '../services/team-data-service.service';

@Component({
  selector: 'app-team-detail',
  templateUrl: './team-detail.page.html',
  styleUrls: ['./team-detail.page.scss'],
})
export class TeamDetailPage implements OnInit {
  team = {};

  constructor(private route: ActivatedRoute, private teamService: TeamDataServiceService) {
    this.route.params.forEach((param: Params) => {
      this.team = this.teamService.getTeam(+param.id);
      console.log(this.team);
    });
  }

  ngOnInit() {
  }
}
